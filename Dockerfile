# Stage 1
FROM node:latest as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
COPY default.conf /app
RUN npm install --force
COPY . /app
RUN npm audit fix --force
RUN npm run build --prod
# Stage 2
FROM nginx:latest
USER root
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-step /app/dist/recruiter-front /usr/share/nginx/html
COPY --from=build-step /app/default.conf /etc/nginx/conf.d/default.conf
