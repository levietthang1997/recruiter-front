import {Injectable} from "@angular/core";

@Injectable()
export class GlobalConstants {
  public static IS_PROD: boolean = false;
  public static readonly API_ENDPOINT: string = GlobalConstants.IS_PROD ? 'https://api.kttk.xyz/api/v1/' : 'http://localhost:9999/api/v1/';
  public static readonly API_ENDPOINT_URL: string = GlobalConstants.IS_PROD ? 'https://api.kttk.xyz' : 'http://localhost:9999';
  public static readonly DOMAIN: string = GlobalConstants.IS_PROD ? 'https://api.kttk.xyz' : 'http://localhost:9999';
}
