import {Injectable} from "@angular/core";

@Injectable()
export class AppParams {
  public static readonly LIST_SIZE_COMPANY = [
    {
      id: 100,
      name: '10-50 người lao động'
    },
    {
      id: 1,
      name: '1-10 người lao động'
    },
    {
      id: 2,
      name: '11-50 người lao động'
    },
    {
      id: 3,
      name: '51-200 người lao động'
    },
    {
      id: 4,
      name: '201-500 người lao động'
    },
    {
      id: 5,
      name: '501-1000 người lao động'
    },
    {
      id: 6,
      name: '1001-5000 người lao động'
    },
    {
      id: 7,
      name: '5001-10000 người lao động'
    },
    {
      id: 8,
      name: '+10001 người lao động'
    }
  ];
  public static readonly LIST_CAREER_COMPANY = [
    {
      id: 1,
      name: 'Designer'
    },
    {
      id: 2,
      name: 'IT'
    },
    {
      id: 3,
      name: 'Sales'
    }
  ];
  public static readonly LIST_JOB_TYPE = [
    {
      id: null,
      name: 'All types'
    },
    {
      id: 2,
      name: 'IT'
    },
    {
      id: 3,
      name: 'Non-IT'
    },
    {
      id: 4,
      name: 'Highlevel'
    }
  ];
  public static readonly LIST_JOB_STATUS = [
    {
      id: null,
      name: 'All'
    },
    {
      id: 2,
      name: 'Draft'
    },
    {
      id: 3,
      name: 'Pending'
    },
    {
      id: 4,
      name: 'Unaccepeted'
    },
    {
      id: 5,
      name: 'Disputed'
    },
    {
      id: 6,
      name: 'Active'
    },
    {
      id: 7,
      name: 'Closed'
    },
    {
      id: 8,
      name: 'Pause'
    }
  ];
}
