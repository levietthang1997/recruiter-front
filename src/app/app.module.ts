import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {IconsProviderModule} from './icons-provider.module';
import {DemoNgZorroAntdModule} from "./ng-zorro-antd.module";
import {en_US, NZ_I18N} from "ng-zorro-antd/i18n";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import {JobListComponent} from "./admin/form/job/job-list/job-list.component";
import {CreateJobComponent} from "./admin/form/job/create-job/create-job.component";
import {UserManagementComponent} from "./admin/form/user-management/user-management.component";
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./guard/AuthGuard";
import {Oauth2HttpInterceptor} from "./admin/interceptor/Oauth2HttpInterceptor";
import {RegisterComponent} from "./register/register.component";
import {GlobalConstants} from "./constants/GlobalConstants";
import {AdminModule} from "./admin/admin.module";
import {PdfViewerModule} from "ng2-pdf-viewer";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const customLanguagePack = {
  en_US,
}

customLanguagePack.en_US.Pagination.items_per_page = "bản ghi";

registerLocaleData(en);

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        realm: 'recruitery_realm',
        url: 'https://sso.kttk.xyz/auth',
        clientId: 'recruiter-frontend',
      },
      initOptions: {
        onLoad: 'check-sso',
        redirectUri: GlobalConstants.IS_PROD ? 'https://app.kttk.xyz/admin' : 'http://localhost:4200/admin',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html'
      },
      enableBearerInterceptor: false
    }).catch(reason => {

    })
}

@NgModule({
  declarations: [
    AppComponent, JobListComponent, CreateJobComponent, UserManagementComponent, LoginComponent, RegisterComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        IconsProviderModule,
        DemoNgZorroAntdModule,
        ReactiveFormsModule,
        KeycloakAngularModule,
        PdfViewerModule,
        AdminModule,
      FontAwesomeModule
    ],
  providers: [{provide: NZ_I18N, useValue: customLanguagePack},
    KeycloakService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Oauth2HttpInterceptor,
      multi: true
    }
    ,AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule  {
}
