import {Component, OnInit} from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserManagementService} from "../admin/services/user-management.service";
import {NzModalService} from "ng-zorro-antd/modal";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  isSpinning = false;
  formRegisCompany!: FormGroup;
  formRegisHeadhunt!: FormGroup;
  registerIndex = 0;

  constructor(private keycloak: KeycloakService, private router: Router, private fb: FormBuilder, private userManagementService: UserManagementService, private modalService: NzModalService) {
  }

  async ngOnInit(): Promise<void> {
    this.formRegisCompany = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: [[], [Validators.required]],
      company: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      phone: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      registrationType: ['COMPANY']
    })

    this.formRegisHeadhunt = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: [[], [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      phone: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      registrationType: ['HEADHUNT']
    })

    if (await this.keycloak.isLoggedIn())
      this.router.navigate(['/admin']);
  }

  doSignup() {
    // Register index = 0 => company login
    this.isSpinning = true;
    this.userManagementService.normalRegister(this.registerIndex == 0 ? this.formRegisCompany.value : this.formRegisHeadhunt.value).subscribe((next) => {
      if (next.status == 200) {
        this.modalService.success({
          nzTitle: 'Thông báo',
          nzContent: 'Đăng ký thành công!',
          nzOkText: 'OK'
        });
        this.router.navigate(["/login"])
      } else {
        this.modalService.error({
          nzTitle: 'Thông báo',
          nzContent: next.message,
          nzOkText: 'OK'
        });
        this.isSpinning = false;
      }
    }, (error) => {
      this.modalService.error({
        nzTitle: 'Thông báo',
        nzContent: 'Đã có lỗi xảy ra, vui lòng thử lại',
        nzOkText: 'OK'
      });
      this.isSpinning = false;
    }, () => {
      this.formRegisCompany.reset({
        registrationType: 'COMPANY'
      });
      this.formRegisHeadhunt.reset({
        registrationType: 'HEADHUNT'
      });
      this.isSpinning = false;
    })
  }

  changeRegisType(event: any) {
    this.formRegisCompany.reset({
      registrationType: 'COMPANY'
    });
    this.formRegisHeadhunt.reset({
      registrationType: 'HEADHUNT'
    });
    this.registerIndex = event.index;
  }

}
