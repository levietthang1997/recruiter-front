export interface CompanyProfileDtoV2 {
  id: string;
  companyName: string;
  fileLogoId: string;
  viewLinkLogo: string;
}
