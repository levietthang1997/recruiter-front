export interface BaseResponseModelV2<T> {
  status?: number,
  message?: string,
  data?: T[]
}
