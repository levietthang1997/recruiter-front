export interface CompanyProfileDtoModel {
  id: number;
  companyName: string;
  companyPhone: string;
  companyAddress: string;
  companySize: number;
  companyEmail: string;
  ccMail: string;
  website: string;
  workingHours: number;
  fileLogoId: string;
  viewLinkLogo: string;
  description: string;
  thumbnailLogoId: string;
  viewLinkThumbnail: string;
  thumbnailDescription: string;
  linkedIn: string;
  facebook: string;
  firstNameRepresentative: string;
  lastNameRepresentative: string;
  headline: string;
  emailRepresentative: string;
  phoneRepresentative: string;
}
