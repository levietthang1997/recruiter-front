import {Skills_jobsModel} from "./skills_jobs.model";
import {Locations_jobModel} from "./locations_job.model";
import {LevelsJobModel} from "./levels-job.model";
import {CompanyProfileDtoModel} from "./company-profile-dto.model";

export interface JobInfoDtoV3 {
  id: number;
  isPlatform: string;
  title: string;
  skills: Skills_jobsModel[];
  isNegotiableSalary: string;
  salaryTo: number;
  salaryFrom: number;
  vacancies: number;
  teamSize: number;
  department: string;
  jobType: string;
  locations: Locations_jobModel[];
  address: string;
  jobOverView: string;
  jobRequirement: string;
  prioritySkill: string;
  whyWokingHere: string;
  interviewingProcess: string;
  employeeType: string;
  levels: LevelsJobModel[];
  noticeToHeadhunter: string;
  companyProfile: CompanyProfileDtoModel;
  reward: number;
  priorityJob: number;
  approveStatus: string;
}
