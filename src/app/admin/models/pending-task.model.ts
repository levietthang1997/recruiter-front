export interface PendingTaskModel {
  id: string;
  string1: string;
  string2: string;
  wfStatusCode: string;
  createDate: string;
  type: string;
}
