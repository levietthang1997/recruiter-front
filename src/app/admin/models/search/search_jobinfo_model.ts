import {BasePagingRequestModel} from "../base_paging_request";

export interface SearchJobInfoModel extends BasePagingRequestModel {
  title?: string | undefined | null;
  jobType?: string | undefined | null;
  pendingTaskStatus?: string | undefined | null;
}
