export interface SearchPendingTaskModel {
  page: number;
  size: number;
  companyId: number;
  wfStatusCd: string
}
