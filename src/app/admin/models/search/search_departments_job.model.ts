import {BasePagingRequestModel} from "../base_paging_request";

export interface SearchDepartmentsJobModel extends BasePagingRequestModel {
  name?:string
}
