import {BasePagingRequestModel} from "../base_paging_request";

export interface SearchLevelsJobModel extends BasePagingRequestModel {
  name?:string
}
