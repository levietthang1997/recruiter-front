import {BasePagingRequestModel} from "../base_paging_request";

export interface SearchSkillJobModel extends BasePagingRequestModel {
  name?:string
}
