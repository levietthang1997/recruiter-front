import {BasePagingRequestModel} from "../base_paging_request";

export interface SearchLocationsJobModel extends BasePagingRequestModel {
  name?:string
}
