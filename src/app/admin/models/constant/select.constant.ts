export const jobTitle = [
  'Senior Project Manager',
  'Centre Manager',
  'Senior PHP Developer',
  'Senior IT Recruiter',
  'SENIOR PHP/LARAVEL',
  'Senior PHP/CakePHP',
  'SENIOR JAVA DEVELOPER',
  'SENIOR PHP DEVELOPERS (Magento)',
  'Senior Mobile Developer (React Native)',
  'Unity Developer'
];

export const classifyJob = [
  'All',
  'IT',
  'Non-IT'
];

export const locations = [
  'Hanoi',
  'Buon Me Thuot',
  'Da Nang',
  'Phu Yen',
  'TP Ho Chi Minh',
  'Can Tho',
  'Vinh',
  'Ninh Binh',
  'Vung Tau',
  'Tay Ninh',
  'An Giang'
];

export const dueDateJob = [
  'All',
  'Tuần trước',
  'Tháng trước',
  '3 tháng trước',
  '6 tháng trước',
  'Năm trước'
];

export const skillJob = [
  'Android',
  'Java',
  'ReactJS',
  'PHP',
  '.NET',
  'iOS',
  'Business',
  'Analyst',
  'QA QC',
  'Tester'
];
