import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {from, Observable, switchMap, tap} from "rxjs";
import {Injectable} from "@angular/core";
import {KeycloakService} from "keycloak-angular";

@Injectable({
  providedIn: 'root'
})
export class Oauth2HttpInterceptor implements HttpInterceptor {

  constructor(private keycloak: KeycloakService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.keycloak.getToken()).pipe(
      switchMap(token => {
      if (token) {
        const headers = req.headers
          .set('Authorization', 'Bearer ' + token)
        const requestClone = req.clone({
          headers
        });
        return next.handle(requestClone);
      } else {

      }
      return next.handle(req);
    }));
  }

}
