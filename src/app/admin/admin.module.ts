import {NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminComponent} from "./admin.component";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import en from "@angular/common/locales/en";
import {HttpClientModule} from "@angular/common/http";
import {UserManagementService} from "./services/user-management.service";
import {IconsProviderModule} from "../icons-provider.module";
import {DemoNgZorroAntdModule} from "../ng-zorro-antd.module";
import {SkillsJobManagementComponent} from './form/job/skills-job-management/skills-job-management.component';
import {LocationsJobManagementComponent} from './form/job/positions-job-management/locations-job-management.component';
import {
  DepartmentJobManagementComponent
} from './form/job/department-job-management/department-job-management.component';
import {LevelsJobManagementComponent} from './form/job/levels-job-management/levels-job-management.component';
import {AccountCompanySettingComponent} from './form/account-company-setting/account-company-setting.component';
import {CompanyProfileComponent} from './form/account-company-setting/company-profile/company-profile.component';
import {
  DepartmentCompanySettingComponent
} from './form/account-company-setting/department-company-setting/department-company-setting.component';
import {
  SlidesShowCompanyComponent
} from './form/account-company-setting/slides-show-company/slides-show-company.component';
import {AngularEditorModule} from "@kolkov/angular-editor";
import {CompanyIndustryComponent} from "./form/config-company/company-industry/company-industry.component";
import {CompanyBenefitComponent} from "./form/config-company/company-benefit/company-benefit.component";
import {PendingTaskComponent} from "./form/pending-task/pending-task.component";
import {ListJobDetailComponent} from './form/job/list-job-detail/list-job-detail.component';
import {SearchJobComponent} from './form/job/search-job/search-job.component';
import {PendingTaskDetailComponent} from "./form/pending-task-detail/pending-task-detail.component";
import {CandidatesListComponent} from "./form/candidates/candidates-list/candidates-list.component";
import {en_US, NZ_I18N} from "ng-zorro-antd/i18n";
import {
  CandidateModalTemplateComponent
} from './form/candidates/candidate-modal-template/candidate-modal-template.component';
import {CandidateModalDetailComponent} from './form/candidates/candidate-modal-detail/candidate-modal-detail.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import {FooterDrawerComponent} from './form/candidates/footer-drawer/footer-drawer.component';
import {ReferralsLandingComponent} from "./form/referrals/referrals-landing/referrals-landing.component";
import {ReferralsListComponent} from "./form/referrals/referrals-list/referrals-list.component";
import {ReferralsCategoryComponent} from "./form/referrals/referrals-category/referrals-category.component";
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {
  CompanyHeadhuntSettingComponent
} from './form/account-headhunt-setting/company-headhunt-setting/company-headhunt-setting.component';
import {
  ApplicationHeadhuntSettingComponent
} from './form/account-headhunt-setting/application-headhunt-setting/application-headhunt-setting.component';
import {
  MemberHeadhuntSettingComponent
} from './form/account-headhunt-setting/member-headhunt-setting/member-headhunt-setting.component';
import {
  HeadhuntIdentitySettingComponent
} from './form/account-headhunt-setting/headhunt-identity-setting/headhunt-identity-setting.component';
import {AccountHeadhuntSettingComponent} from "./form/account-headhunt-setting/account-headhunt-setting.component";

registerLocaleData(en);

@NgModule({
  imports: [
    AdminRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    IconsProviderModule,
    HttpClientModule,
    DemoNgZorroAntdModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    AngularEditorModule,
    PdfViewerModule,
    FontAwesomeModule
  ],
  declarations: [
    AdminComponent,
    SkillsJobManagementComponent,
    LocationsJobManagementComponent,
    DepartmentJobManagementComponent,
    LevelsJobManagementComponent,
    CandidatesListComponent,
    AccountCompanySettingComponent,
    CompanyProfileComponent,
    DepartmentCompanySettingComponent,
    SlidesShowCompanyComponent,
    CompanyIndustryComponent,
    CompanyBenefitComponent,
    PendingTaskComponent,
    ListJobDetailComponent,
    SearchJobComponent,
    PendingTaskComponent,
    PendingTaskDetailComponent,
    CandidateModalTemplateComponent,
    CandidateModalDetailComponent,
    ReferralsLandingComponent,
    ReferralsListComponent,
    ReferralsCategoryComponent,
    FooterDrawerComponent,
    CompanyHeadhuntSettingComponent,
    ApplicationHeadhuntSettingComponent,
    MemberHeadhuntSettingComponent,
    AccountHeadhuntSettingComponent,
    HeadhuntIdentitySettingComponent
  ],
  exports: [AdminComponent, ListJobDetailComponent, SearchJobComponent],
  providers: [UserManagementService, {provide: NZ_I18N, useValue: en_US}]
})
export class AdminModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }
}
