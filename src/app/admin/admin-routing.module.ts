import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from "./admin.component";
import {UserManagementComponent} from "./form/user-management/user-management.component";
import {CreateJobComponent} from "./form/job/create-job/create-job.component";
import {JobListComponent} from "./form/job/job-list/job-list.component";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {Oauth2HttpInterceptor} from "./interceptor/Oauth2HttpInterceptor";
import {
  DepartmentJobManagementComponent
} from "./form/job/department-job-management/department-job-management.component";
import {LevelsJobManagementComponent} from "./form/job/levels-job-management/levels-job-management.component";
import {LocationsJobManagementComponent} from "./form/job/positions-job-management/locations-job-management.component";
import {SkillsJobManagementComponent} from "./form/job/skills-job-management/skills-job-management.component";
import {AccountCompanySettingComponent} from "./form/account-company-setting/account-company-setting.component";
import {CompanyIndustryComponent} from "./form/config-company/company-industry/company-industry.component";
import {CompanyBenefitComponent} from "./form/config-company/company-benefit/company-benefit.component";
import {PendingTaskComponent} from "./form/pending-task/pending-task.component";
import {PendingTaskDetailComponent} from "./form/pending-task-detail/pending-task-detail.component";
import {CandidatesListComponent} from "./form/candidates/candidates-list/candidates-list.component";
import {ReferralsCategoryComponent} from "./form/referrals/referrals-category/referrals-category.component";
import {ReferralsLandingComponent} from "./form/referrals/referrals-landing/referrals-landing.component";
import {AccountHeadhuntSettingComponent} from "./form/account-headhunt-setting/account-headhunt-setting.component";

const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      {
        path: 'user-management', component: UserManagementComponent
      },
      {
        path: 'job-list', component: JobListComponent
      },
      {
        path: 'job/:id/editor', component: CreateJobComponent
      },
      {
        path: 'department-job', component: DepartmentJobManagementComponent
      },
      {
        path: 'levels-job', component: LevelsJobManagementComponent
      },
      {
        path: 'positions-job', component: LocationsJobManagementComponent
      },
      {
        path: 'skills-job', component: SkillsJobManagementComponent
      },
      {
        path: 'account-company-setting', component: AccountCompanySettingComponent
      },
      {
        path: 'account-headhunt-setting', component: AccountHeadhuntSettingComponent
      },
      {
        path: 'company-industry-setting', component: CompanyIndustryComponent
      },
      {
        path: 'company-benefit-setting', component: CompanyBenefitComponent
      },
      {
        path: 'pending-task', component: PendingTaskComponent
      },
      {
        path: 'pending-task-detail/:pendingTaskId', component: PendingTaskDetailComponent
      },
      {
        path: 'candidates', component: CandidatesListComponent
      },
      {
        path: 'referrals', component: ReferralsLandingComponent
      },
      {
        path: 'referrals/:category', component: ReferralsCategoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: Oauth2HttpInterceptor
    }
  ]
})
export class AdminRoutingModule {
}
