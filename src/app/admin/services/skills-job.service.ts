import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";
import {BaseResponseModel} from "../models/base_response.model";
import {BasePagingResponseModel} from "../models/base_paging_response_model";
import {SearchSkillJobModel} from "../models/search/search_skills_job.model";
import {Skills_jobsModel} from "../models/skills_jobs.model";
import {Locations_jobModel} from "../models/locations_job.model";
import {SelectModel} from "../models/jobs.model";

@Injectable({
  providedIn: 'root',
})
export class SkillsJobService {

  private readonly API_SKILL_INFO_NAME = 'skills';
  private readonly API_ROLE_NAME = 'roles';

  constructor(private http: HttpClient) {
  }

  getAllUserInfo(query: SearchSkillJobModel): Observable<BasePagingResponseModel<Skills_jobsModel>> {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + this.API_SKILL_INFO_NAME + "/search", query)
      .pipe(
        map((response:BasePagingResponseModel<Skills_jobsModel>) => {
          return response;
        })
      )
  }

  adminCreateUser(value: any): Observable<BaseResponseModel<Skills_jobsModel>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_SKILL_INFO_NAME + "/create", value).pipe(
      map((response:BaseResponseModel<Skills_jobsModel>) => {
        return response;
      })
    )
  }

  adminDeleteUser(value: any): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_SKILL_INFO_NAME + "/delete", value).pipe(
      map((response: any) => {
        return response;
      })
    )
  }

  adminEditUser(value:any): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_SKILL_INFO_NAME + "/update", value).pipe(
      map((response: any) => {
        return response;
      })
    )
  }

  getAllSkillInfo(): Observable<BasePagingResponseModel<SelectModel>> {
    return this.http.get(GlobalConstants.API_ENDPOINT + this.API_SKILL_INFO_NAME + "/all");
  }
}
