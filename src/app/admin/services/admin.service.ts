import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";

@Injectable({
  providedIn: 'root',
})
export class AdminService {

  constructor(private http: HttpClient) {
  }

  uploadSingleFile(data: any) {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + 'file-management/single-upload', data)
  }
}
