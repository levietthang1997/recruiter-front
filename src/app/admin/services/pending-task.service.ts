import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";
import {CompanyProfileDtoV2} from "../models/company-profile-dto-v2";
import {BaseResponseModelV2} from "../models/base-response.modelv2";
import {BasePagingResponseModel} from "../models/base_paging_response_model";
import {PendingTaskModel} from "../models/pending-task.model";
import {SearchPendingTaskModel} from "../models/search/search-pending-task.model";

@Injectable({
  providedIn: 'root',
})
export class PendingTaskService {

  API_PENDING_TASK: string = 'pending-task';
  API_COMPANY: string = 'company';

  constructor(private http: HttpClient) {
  }

  getAllCompanyInfo(): Observable<BaseResponseModelV2<CompanyProfileDtoV2>> {
    return this.http
      .get(GlobalConstants.API_ENDPOINT + this.API_COMPANY + "/all-company")
      .pipe(
        map((response:BaseResponseModelV2<CompanyProfileDtoV2>) => {
          return response;
        })
      )
  }

  getAllPendingTaskJob(searchPendingTask: SearchPendingTaskModel): Observable<BasePagingResponseModel<PendingTaskModel>> {
    return this.http.post(
      GlobalConstants.API_ENDPOINT + this.API_PENDING_TASK + "/get-pending-job",
      searchPendingTask
    ).pipe(
      map((response:BasePagingResponseModel<PendingTaskModel>) => {
        return response;
      })
    )
  }

  approvePendingTaskJob(value: any) {
    return this.http.post(
      GlobalConstants.API_ENDPOINT + this.API_PENDING_TASK + "/update-status-pending-job",
      value
    ).pipe(
      map((response: any) => {
        return response;
      })
    )
  }
}
