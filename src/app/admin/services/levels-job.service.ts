import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";
import {BaseResponseModel} from "../models/base_response.model";
import {BasePagingResponseModel} from "../models/base_paging_response_model";
import {Locations_jobModel} from "../models/locations_job.model";
import {SearchLocationsJobModel} from "../models/search/search_locations_job.model";
import {LevelsJobModel} from "../models/levels-job.model";
import {SearchLevelsJobModel} from "../models/search/search_levels_job.model";
import {SelectModel} from "../models/jobs.model";

@Injectable({
  providedIn: 'root',
})
export class LevelsJobService {

  private readonly API_LEVEL_INFO_NAME = 'level';
  private readonly API_ROLE_NAME = 'roles';

  constructor(private http: HttpClient) {
  }

  getAllUserInfo(query: SearchLevelsJobModel): Observable<BasePagingResponseModel<LevelsJobModel>> {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + this.API_LEVEL_INFO_NAME + "/search", query)
      .pipe(
        map((response:BasePagingResponseModel<LevelsJobModel>) => {
          return response;
        })
      )
  }

  adminCreateUser(value: any): Observable<BaseResponseModel<LevelsJobModel>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_LEVEL_INFO_NAME + "/create", value).pipe(
      map((response:BaseResponseModel<LevelsJobModel>) => {
        return response;
      })
    )
  }

  adminDeleteUser(value: any): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_LEVEL_INFO_NAME + "/delete", value).pipe(
      map((response: any) => {
        return response;
      })
    )
  }

  adminEditUser(value:any): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_LEVEL_INFO_NAME + "/update", value).pipe(
      map((response: any) => {
        return response;
      })
    )
  }

  getAllLevelInfo(): Observable<BasePagingResponseModel<SelectModel>> {
    return this.http.get(GlobalConstants.API_ENDPOINT + this.API_LEVEL_INFO_NAME + "/all").pipe(
      map((response: any) => {
        return response;
      })
    );
  }
}
