import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";
import {CompanyProfileDtoV2} from "../models/company-profile-dto-v2";
import {BaseResponseModelV2} from "../models/base-response.modelv2";

@Injectable({
  providedIn: 'root',
})
export class CompanyService {

  API_COMPANY_PROFILE: string = 'company';

  constructor(private http: HttpClient) {
  }

  getAllCompanyInfo(): Observable<BaseResponseModelV2<CompanyProfileDtoV2>> {
    return this.http
      .get(GlobalConstants.API_ENDPOINT + this.API_COMPANY_PROFILE + "/all-company")
      .pipe(
        map((response:BaseResponseModelV2<CompanyProfileDtoV2>) => {
          return response;
        })
      )
  }
}
