import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";

@Injectable({
  providedIn: 'root',
})
export class CandidateService {

  constructor(private http: HttpClient) {
  }

  public downloadFile(url: string): any {
    return this.http.get(url, { responseType: 'blob' });
  }
}
