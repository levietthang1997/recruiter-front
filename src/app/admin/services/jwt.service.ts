import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {KeycloakService} from "keycloak-angular";

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  token = new BehaviorSubject<any>(null);
  

  constructor(
    private keycloak: KeycloakService
  ) {
    this.keycloak.getToken().then(data => this.token.next(data)).catch(console.log)
  }


}
