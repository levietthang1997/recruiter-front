import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../constants/GlobalConstants";
import {SearchJobInfoModel} from "../models/search/search_jobinfo_model";
import {map, Observable} from "rxjs";
import {BaseResponseModelV2} from "../models/base-response.modelv2";
import {CompanyProfileDtoV2} from "../models/company-profile-dto-v2";
import {BaseResponseModel} from "../models/base_response.model";
import {JobInfoDtoV3} from "../models/job-info-dto-v3";

@Injectable({
  providedIn: 'root',
})
export class JobService {

  private readonly API_JOB = 'job';

  constructor(private http: HttpClient) {
  }

  createJob(data: any) {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + this.API_JOB + "/company-create", data)
  }

  update(data: any) {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + this.API_JOB + "/company-update", data)
  }

  getJob(id: number) {
    return this.http
      .get(GlobalConstants.API_ENDPOINT + this.API_JOB + "/get/" + id)
  }

  getJobPendingDetail(pendingId: string): Observable<BaseResponseModel<JobInfoDtoV3>> {
    return this.http.get(GlobalConstants.API_ENDPOINT + this.API_JOB + "/job-pending-detail/" + pendingId)
      .pipe(
        map((response:BaseResponseModel<JobInfoDtoV3>) => {
          return response;
        })
      )
  }

  getAllJob(data: any) {
    return this.http
      .post(GlobalConstants.API_ENDPOINT + this.API_JOB + "/company-job", data);
  }
}
