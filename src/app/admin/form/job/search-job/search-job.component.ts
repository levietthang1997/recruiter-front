import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AppParams} from "../../../../constants/app-params";

@Component({
  selector: 'app-search-job',
  templateUrl: './search-job.component.html',
  styleUrls: ['./search-job.component.css']
})
export class SearchJobComponent implements OnInit {
  formSearch: any;
  listOptionStatus: any;
  listOptionJobType: any;
  @Output() submitSearch = new EventEmitter();

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.listOptionStatus = AppParams.LIST_JOB_STATUS;
    this.listOptionJobType = AppParams.LIST_JOB_TYPE;
    this.formSearch = this.fb.group({
      title: ['', Validators.required],
      jobType: [null, Validators.required],
      pendingTaskStatus: [null, Validators.required],
    });
  }

  search() {
    this.submitSearch.emit(this.formSearch.value);
  }

}
