import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserInfoModel} from "../../../models/user_info.model";
import {RoleModel} from "../../../models/role.model";
import {NzModalService} from "ng-zorro-antd/modal";
import {UserManagementService} from "../../../services/user-management.service";
import {BaseResponseModel} from "../../../models/base_response.model";
import {SearchUserInfoModel} from "../../../models/search/search_userinfo_model";
import {BasePagingResponseModel} from "../../../models/base_paging_response_model";
import {ActivatedRoute, Router} from "@angular/router";
import {SearchJobInfoModel} from "../../../models/search/search_jobinfo_model";
import {JobService} from "../../../services/job.service";

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  formSearch!: FormGroup;
  formCreate!: FormGroup;
  formEdit!: FormGroup;
  controlArray: Array<{ index: number; show: boolean }> = [];
  isCollapse = true;
  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: readonly any[] = [];
  listOfCurrentPageData: readonly UserInfoModel[] = [];
  setOfCheckedId = new Set<number>();
  passwordVisible = false;
  password?: string;
  isVisible = false;
  isVisibleEdit = false;
  isConfirmLoading = false;
  listOfRole: readonly RoleModel[] = [];
  isSpinning = false;
  isSaving = false;
  //pagination
  totalData:number = 0;
  pageIndex:number = 1;
  pageSize:number = 10;

  listJob: any;
  loadingJob = false;

  constructor(
    private userManagementService: UserManagementService,
    private jobService: JobService,
    private routeActive: ActivatedRoute,
    protected router: Router
  ) {}

  toggleCollapse(): void {
  }

  resetFormSearch(): void {
    this.formSearch.reset();
  }

  ngOnInit(): void {

    this.loadingJobInfoInit();
  }

  showModal1(): void {
    this.router.navigate(['/admin/job/0/editor']);
  }

  submitForm() {

  }

  loadingJobInfoInit() {
    const param = {
      "page": 0,
      "size": 10,
      "title": null,
      "jobType": null,
      "pendingTaskStatus": null
    }
    this.jobService.getAllJob(param).subscribe((data: any) => {
      this.listJob = data.data.data;
      this.loadingJob = false;
      console.log(data);
    });
  }

  loadingJobInfo(event: any) {
    const param = {
      "page": 0,
      "size": 100,
      "title": event.title,
      "jobType": event.jobType,
      "pendingTaskStatus": event.pendingTaskStatus
    }
    this.jobService.getAllJob(param).subscribe((data: any) => {
      this.listJob = data.data.data;
      console.log(data);
    });
  }

}
