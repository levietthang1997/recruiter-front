import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RoleModel} from "../../../models/role.model";
import {NzModalService} from "ng-zorro-antd/modal";
import {KeycloakService} from "keycloak-angular";
import {BaseResponseModel} from "../../../models/base_response.model";
import {BasePagingResponseModel} from "../../../models/base_paging_response_model";
import {Locations_jobModel} from "../../../models/locations_job.model";
import {LocationsJobService} from "../../../services/locations-job.service";
import {SearchLocationsJobModel} from "../../../models/search/search_locations_job.model";

@Component({
  selector: 'app-positions-job-management',
  templateUrl: './locations-job-management.component.html',
  styleUrls: ['./locations-job-management.component.scss']
})
export class LocationsJobManagementComponent implements OnInit {

  formSearch!: FormGroup;
  formCreate!: FormGroup;
  formEdit!: FormGroup;
  controlArray: Array<{ index: number; show: boolean }> = [];
  isCollapse = true;
  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: readonly Locations_jobModel[] = [];
  listOfCurrentPageData: readonly Locations_jobModel[] = [];
  setOfCheckedId = new Set<number>();
  password?: string;
  isVisible = false;
  isVisibleEdit = false;
  isConfirmLoading = false;
  isSpinning = false;
  isSaving = false;
  //pagination
  totalData:number = 0;
  pageIndex:number = 1;
  pageSize:number = 10;

  constructor(private fb: FormBuilder, private modalService: NzModalService, private locationsJobService: LocationsJobService, private keycloak: KeycloakService) {}

  toggleCollapse(): void {
    this.isCollapse = !this.isCollapse;
    this.controlArray.forEach((c, index) => {
      c.show = this.isCollapse ? index < 3 : true;
    });
  }

  resetFormSearch(): void {
    this.formSearch.reset();
  }

  ngOnInit(): void {
    this.formSearch = this.fb.group({
      name: [null, []],
    });

    this.formCreate = this.fb.group({
      name: ['', [Validators.required]],
    });

    this.formEdit = this.fb.group({
      name: ['', [Validators.required]],
      id: [null, []]
    });

    this.isSpinning = true;
    this.loadingUserInfo();
    console.log("user data: ", this.keycloak.getUserRoles(true))
  }

  showModal1(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    this.isSaving = true;

    // do save userInfo
    this.locationsJobService.adminCreateUser(this.formCreate.value).subscribe((next) => {
      if (next.status == 200) {
        this.modalService.success({
          nzTitle: 'Thông báo',
          nzContent: 'Thêm mới vị trí thành công',
          nzOkText: 'OK'
        });
        this.isVisible = false;
        this.formCreate.reset();
        this.loadingUserInfo();
      } else {
        this.modalService.error({
          nzTitle: 'Thông báo',
          nzContent: next.message,
          nzOkText: 'OK'
        });
      }
    }, (error) => {
      this.modalService.error({
        nzTitle: 'Thông báo',
        nzContent: 'Đã có lỗi xảy ra, vui lòng thử lại',
        nzOkText: 'OK'
      });
    }, () => {
      this.isSaving = false;
      this.isConfirmLoading = false;
    })
  }

  handleCancel(): void {
    this.isVisible = false;
    this.formCreate.reset();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter(data => this.setOfCheckedId.has(data['id']));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }

  submitForm() {

  }

  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      }
    },
    {
      text: 'Select Odd Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 !== 0));
        this.refreshCheckedStatus();
      }
    },
    {
      text: 'Select Even Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 === 0));
        this.refreshCheckedStatus();
      }
    }
  ];

  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: readonly Locations_jobModel[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  showDeleteConfirm(): void {
    console.log({
      listId: [...this.setOfCheckedId.values()]
    })
    this.modalService.confirm({
      nzTitle: `Bạn có muốn xoá ${this.setOfCheckedId.size} dữ liệu này?`,
      nzOkText: 'Đồng ý',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => {
        this.locationsJobService.adminDeleteUser({
          listId: [...this.setOfCheckedId.values()]
        }).subscribe((response: BaseResponseModel<Object>) => {
          if (200 == response.status) {
            this.setOfCheckedId.clear();
            this.refreshCheckedStatus();
            this.modalService.success({
              nzTitle: 'Thông báo',
              nzContent: 'Xoá thành công',
              nzOkText: 'OK'
            });
            this.loadingUserInfo();
          } else {
            this.modalService.error({
              nzTitle: 'Thông báo',
              nzContent: 'Xoá thất bại',
              nzOkText: 'OK'
            });
          }
        }, (error) => {
          this.modalService.error({
            nzTitle: 'Thông báo',
            nzContent: 'Có lỗi xảy ra, vui lòng thử lại',
            nzOkText: 'OK'
          });
        });
      },
      nzCancelText: 'Huỷ',
      nzOnCancel: () => console.log('Cancel')
    });
  }

  loadingUserInfo() : void {
    let query: SearchLocationsJobModel = {
      page: this.pageIndex - 1,
      size: this.pageSize,
      name: this.isEmpty(this.formSearch.controls['name'].value) ? null : this.formSearch.controls['name'].value,
    }
    this.locationsJobService.getAllUserInfo(query).subscribe(
      (value: BasePagingResponseModel<Locations_jobModel>) => {
        this.isSpinning = false;
        this.listOfData = value.data?.data? value.data.data : [];
        this.totalData = value.data?.total? value.data.total : 0;
      },
      error => {
        console.log(error)
        this.isSpinning = false;
      },
    )
  }

  isEmpty(str:string) {
    return (!str || str.length === 0 );
  }

  changePageIndex($event: number) {
    this.pageIndex = $event.valueOf();
    this.loadingUserInfo();
  }

  changePageSize($event: number) {
    this.pageSize = $event.valueOf();
    console.log('pageSize', this.pageSize)
    this.loadingUserInfo();
  }

  showModal2(data: Locations_jobModel) {
    this.formEdit.controls['id'].disable();
    this.formEdit.controls['id'].setValue(data.id);
    this.formEdit.controls['name'].setValue(data.name);
    this.isVisibleEdit = true;
  }

  handleCancelEdit() {
    this.isVisibleEdit = false;
    this.formEdit.reset();
  }

  handleOkEdit() {
    this.isConfirmLoading = true;
    this.isSaving = true;

    // do update userInfo
    this.locationsJobService.adminEditUser(this.formEdit.getRawValue()).subscribe((next) => {
      if (next.status == 200) {
        this.modalService.success({
          nzTitle: 'Thông báo',
          nzContent: 'Sửa vị trí thành công',
          nzOkText: 'OK'
        });
        this.isVisibleEdit = false;
        this.formEdit.reset();
        this.loadingUserInfo();
      } else {
        this.modalService.error({
          nzTitle: 'Thông báo',
          nzContent: next.message,
          nzOkText: 'OK'
        });
      }
    }, (error) => {
      this.modalService.error({
        nzTitle: 'Thông báo',
        nzContent: 'Đã có lỗi xảy ra, vui lòng thử lại',
        nzOkText: 'OK'
      });
    }, () => {
      this.isSaving = false;
      this.isConfirmLoading = false;
    })
  }

}
