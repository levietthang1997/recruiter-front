import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-list-job-detail',
  templateUrl: './list-job-detail.component.html',
  styleUrls: ['./list-job-detail.component.scss']
})
export class ListJobDetailComponent implements OnInit, OnChanges {
  @Input() listJob: any;

  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.listJob && this.listJob.length) {
      for (const j of this.listJob) {
        if (j.locations && j.locations.length) {
          j.locationStr = j.locations.map((je: any) => je.name).join(',');
        }
      }
    }
  }

  updateJob(id?: number) {
    this.route.navigate(['job', id , 'editor']);
  }

}
