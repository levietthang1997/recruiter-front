import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SelectModel} from "../../../models/jobs.model";
import {LocationsJobService} from "../../../services/locations-job.service";
import {LevelsJobService} from "../../../services/levels-job.service";
import {SkillsJobService} from "../../../services/skills-job.service";
import {JobService} from "../../../services/job.service";
import {ActivatedRoute, Route, Router} from "@angular/router";

@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.css']
})
export class CreateJobComponent implements OnInit {

  validateForm!: FormGroup;
  isSpinning = false;
  isPlatform = true;
  checked = true;
  skills: any;
  locations: any;
  levels: any;
  form: any;
  id: number = 1;
  loading = true;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private routeL: Router,
    private locationsJobService: LocationsJobService,
    private levelsJobService: LevelsJobService,
    private skillsJobService: SkillsJobService,
    private jobService: JobService
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    if (this.id == 0) {
      this.form = this.fb.group({
        title: ['', Validators.required],
        skills: [[], Validators.required],
        isPlatform: [true, Validators.required],
        isNegotiableSalary: [false, Validators.required],
        salaryFrom: [0,Validators.required],
        salaryTo: [0,Validators.required],
        vacancies: [null,Validators.required],
        teamSize: [null,Validators.required],
        department: [null,Validators.required],
        jobType: [null,Validators.required],
        locations: [[],Validators.required],
        address: [null,Validators.required],
        jobOverView: [null,Validators.required],
        jobRequirement: [null,Validators.required],
        prioritySkill: [null,Validators.required],
        interviewingProcess: [null,Validators.required],
        noticeToHeadhunter: [null,Validators.required],
        whyWokingHere: [null,Validators.required],
        employeeType: [null,Validators.required],
        levels: [[],Validators.required],
      });
      this.loading = false;
    } else {

      this.jobService.getAllJob({page: 0, size:300}).subscribe((dataMap: any) => {
        const data = dataMap.data.data.find((job: any) => job.id == this.id);
        data.skills = data.skills.map((sk: any) => sk.id);
        data.locations = data.locations.map((l: any) => l.id);
        data.levels = data.levels.map((l: any) => l.id);
        this.isPlatform = data.isPlatform == 'Y';
        console.log(data);
        setTimeout(() => {
          this.form = this.fb.group({
            title: [data.title, Validators.required],
            skills: [data.skills, Validators.required],
            salaryFrom: [data.salaryFrom,Validators.required],
            salaryTo: [data.salaryTo,Validators.required],
            isPlatform: [data.isPlatform,Validators.required],
            isNegotiableSalary: [data.isNegotiableSalary == 'Y', Validators.required],
            vacancies: [data.vacancies,Validators.required],
            teamSize: [data.teamSize,Validators.required],
            department: [data.department,Validators.required],
            jobType: [data.jobType,Validators.required],
            locations: [data.locations,Validators.required],
            address: [data.address,Validators.required],
            jobOverView: [data.jobOverView,Validators.required],
            jobRequirement: [data.jobRequirement,Validators.required],
            prioritySkill: [data.prioritySkill,Validators.required],
            interviewingProcess: [data.interviewingProcess,Validators.required],
            noticeToHeadhunter: [data.noticeToHeadhunter,Validators.required],
            whyWokingHere: [data.whyWokingHere,Validators.required],
            employeeType: [data.employeeType,Validators.required],
            levels: [data.levels,Validators.required]
          });
          this.loading = false;
        }, 1500)

      });
    }
    this.initSelectionCreateJob();
    this.validateForm = this.fb.group({
      formLayout: ['horizontal']
    });
  }

  initSelectionCreateJob(): void {
    this.locationsJobService.getAllLocationInfo().subscribe((data) => {
      this.locations = data?.data;
    });
    this.levelsJobService.getAllLevelInfo().subscribe((data) => {
      this.levels = data?.data;
    });
    this.skillsJobService.getAllSkillInfo().subscribe((data) => {
      this.skills = data?.data;
    });
  }

  submitForm(): void {
    let tempJob = JSON.parse(JSON.stringify(this.form.value));
    tempJob.isPlatform = this.isPlatform ? 'Y' : 'N';
    tempJob.isNegotiableSalary = this.form.get('isNegotiableSalary').value ? 'Y' : 'N';
    if (this.id == 0) {
      this.jobService.createJob(tempJob).subscribe((data) => {
        this.routeL.navigate(['/admin/job-list']);
      });
    } else {
      tempJob.id = this.id;
      console.log('chay vao day')
      this.jobService.update(tempJob).subscribe((data) => {
        this.routeL.navigate(['/admin/job-list']);
      });
    }

  }

  get isHorizontal(): boolean {
    return this.validateForm.controls['formLayout']?.value === 'horizontal';
  }

  formatterVND = (value: number): string => `đ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  parserVND = (value: string): string => value!.replace(/đ\s?|(,*)/g, '');

  t() {
    console.log(this);
  }

}
