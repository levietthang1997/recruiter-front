import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {en_US, NzI18nService} from "ng-zorro-antd/i18n";
import {from, fromEvent, tap} from "rxjs";
interface DataItem {
  date: string;
  status: string;
  job: string;
  candidateName: string;
  email: string;
  phone: string;
  dateProcessing: string;
}

@Component({
  selector: 'app-referrals-landing',
  templateUrl: './referrals-landing.component.html',
  styleUrls: ['./referrals-landing.component.css']
})
export class ReferralsLandingComponent implements OnInit {
  formSearchReferrals = new FormGroup({});
  @ViewChild('firstMenu') firstMenu: ElementRef | undefined;
  locations = [
    {
      id: 1,
      name: 'Hà Nội'
    },
    {
      id: 2,
      name: 'Singapore'
    },
    {
      id: 3,
      name: 'TP Hồ Chí Minh'
    }
  ]

  status = [
    {
      id: 1,
      name: 'Chờ xử lý'
    },
    {
      id: 2,
      name: 'Suggested'
    },
    {
      id: 3,
      name: 'Applied'
    }
  ];
  radioValue = 'A';



  listOfColumn = [
    {
      title: 'Ngày giới thiệu',
      compare: null,
      priority: false
    },
    {
      title: 'Trạng thái',
      compare: null,
      priority: 3
    },
    {
      title: 'Công việc',
      compare: null,
      priority: 2
    },
    {
      title: 'Ứng viên',
      compare: null,
      priority: 1
    },
    {
      title: 'Email',
      compare: null,
      priority: 4
    },
    {
      title: 'Số điện thoại',
      compare: null,
      priority: 5
    },
    {
      title: 'Ngày chờ xử lý',
      compare: null,
      priority: 6
    }
  ];
  listOfData: DataItem[] = [
    {
      date: 'Tháng 3 17, 2022',
      status: 'Từ chối',
      job: 'IT',
      candidateName: 'Dức Anh',
      email: 'anhpdn2@gmail.com',
      phone: '0968045618',
      dateProcessing: '08/08/2022'
    },
    {
      date: 'Tháng 3 18, 2022',
      status:  'Từ chối',
      job: 'IT',
      candidateName: 'Dức Anh',
      email: 'anhpdn2@gmail.com',
      phone: '0968045618',
      dateProcessing: '08/08/2022'
    },
    {
      date: 'Tháng 3 19, 2022',
      status:  'Từ chối',
      job: 'IT',
      candidateName: 'Dức Anh',
      email: 'anhpdn2@gmail.com',
      phone: '0968045618',
      dateProcessing: '08/08/2022'
    },
    {
      date: 'Tháng 3 20, 2022',
      status:  'Từ chối',
      job: 'IT',
      candidateName: 'Dức Anh',
      email: 'anhpdn2@gmail.com',
      phone: '0968045618',
      dateProcessing: '08/08/2022'
    }
  ];

  constructor(
    private i18n: NzI18nService,
    private elem: ElementRef
    ) {
    this.i18n.setLocale(en_US);
  }

  ngOnInit() {
    let marker = this.elem.nativeElement.querySelector('#marker');
    let allMenu = document.querySelectorAll('.nav a');
    fromEvent(allMenu, 'click').subscribe(
      (e: any) => {
        allMenu.forEach(menu => {
          if (menu.classList.contains('active')) menu.classList.remove('active');
        });
        e.target.classList.add('active');
        marker.style.left = e.target?.offsetLeft + 12 + 'px';
        marker.style.width = e.target?.offsetWidth - 24 + 'px';
      }
    )
  }

  onChange() {
    console.log('sdfsdfsdf');
  }
}
