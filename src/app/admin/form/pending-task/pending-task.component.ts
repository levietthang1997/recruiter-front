import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzModalService} from "ng-zorro-antd/modal";
import {RoleModel} from "../../models/role.model";
import {BasePagingResponseModel} from "../../models/base_paging_response_model";
import {KeycloakService} from "keycloak-angular";
import {CompanyProfileDtoV2} from "../../models/company-profile-dto-v2";
import {CompanyService} from "../../services/company.service";
import {GlobalConstants} from "../../../constants/GlobalConstants";
import {PendingTaskModel} from "../../models/pending-task.model";
import {SearchPendingTaskModel} from "../../models/search/search-pending-task.model";
import {PendingTaskService} from "../../services/pending-task.service";

@Component({
  selector: 'app-pending-task',
  templateUrl: 'pending-task.component.html',
  styleUrls: ['pending-task.component.css']
})

export class PendingTaskComponent implements OnInit {

  formSearch!: FormGroup;
  controlArray: Array<{ index: number; show: boolean }> = [];
  isCollapse = true;
  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: readonly PendingTaskModel[] = [];
  listOfCurrentPageData: readonly PendingTaskModel[] = [];
  setOfCheckedId = new Set<any>();
  passwordVisible = false;
  password?: string;
  isVisible = false;
  isVisibleEdit = false;
  isConfirmLoading = false;
  isSpinning = false;
  isSaving = false;
  //pagination
  totalData: number = 0;
  pageIndex: number = 1;
  pageSize: number = 10;

  // get list company
  lstCompanyProfile?: CompanyProfileDtoV2[] = [];
  lstWfStatusCd = [
    {
      code: 'PENDING_APPROVE',
      name: 'Đang chờ phê duyệt'
    },
    {
      code: 'APPROVED',
      name: 'Đã phê duyệt'
    }, {
      code: 'REJECTED',
      name: 'Từ chối'
    }, {
      code: 'CLOSED',
      name: 'Admin đóng'
    }
  ];

  constructor(private fb: FormBuilder, private modalService: NzModalService,
              private pendingTaskService: PendingTaskService, private keycloak: KeycloakService,
              private companyService: CompanyService) {
  }

  toggleCollapse(): void {
    this.isCollapse = !this.isCollapse;
    this.controlArray.forEach((c, index) => {
      c.show = this.isCollapse ? index < 3 : true;
    });
  }

  resetFormSearch(): void {
    this.formSearch.reset();
  }

  ngOnInit(): void {
    this.formSearch = this.fb.group({
      companyId: [null, []],
      wfStatusCd: [null, []]
    });

    this.loadCompany();
    this.loadPendingTask();
  }

  loadCompany() {
    this.isSpinning = true;
    this.companyService.getAllCompanyInfo().subscribe(response => {
      this.isSpinning = false;
      if (response.status == 200) {
        this.lstCompanyProfile = response.data?.map(item => {
          item.viewLinkLogo = GlobalConstants.DOMAIN + item.viewLinkLogo;
          return item;
        });
      }
    }, error => {
      this.isSpinning = false;
    })
  }

  public loadPendingTask() {
    let query: SearchPendingTaskModel = {
      page: this.pageIndex - 1,
      size: this.pageSize,
      companyId: this.isEmpty(this.formSearch.controls['companyId'].value) ? null : this.formSearch.controls['companyId'].value,
      wfStatusCd: this.isEmpty(this.formSearch.controls['wfStatusCd'].value) ? null : this.formSearch.controls['wfStatusCd'].value,
    }
    this.pendingTaskService.getAllPendingTaskJob(query).subscribe(
      (value: BasePagingResponseModel<PendingTaskModel>) => {
        this.isSpinning = false;
        this.listOfData = value.data?.data ? value.data.data : [];
        this.totalData = value.data?.total ? value.data.total : 0;
      },
      error => {
        console.log(error)
        this.isSpinning = false;
      },
    )
  }

  showModal1(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    this.isSaving = true;

    // do save userInfo

  }

  handleCancel(): void {
    this.isVisible = false;
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter(data => this.setOfCheckedId.has(data['id']));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }

  submitForm() {

  }

  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      }
    },
    {
      text: 'Select Odd Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 !== 0));
        this.refreshCheckedStatus();
      }
    },
    {
      text: 'Select Even Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 === 0));
        this.refreshCheckedStatus();
      }
    }
  ];

  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: readonly PendingTaskModel[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  showDeleteConfirm(): void {
    console.log({
      listId: [...this.setOfCheckedId.values()]
    })

  }

  isEmpty(str: string) {
    return (!str || str.length === 0);
  }

  toListRoleName(roles: RoleModel[]) {
    return roles.map(item => item.name).join(",");
  }


  changePageIndex($event: number) {
    this.pageIndex = $event.valueOf();
  }

  changePageSize($event: number) {
    this.pageSize = $event.valueOf();
    console.log('pageSize', this.pageSize)
  }

  showModal2(data: PendingTaskModel) {
    this.isVisibleEdit = true;
  }

  handleCancelEdit() {
    this.isVisibleEdit = false;
  }

  handleOkEdit() {
    this.isConfirmLoading = true;
    this.isSaving = true;

    // do update userInfo

  }

  getPendingTaskName(code: string) {
    let name = this.lstWfStatusCd.filter(item => item.code == code)?.[0]?.name;
    if (name)
      return name;
    else
      return 'UNKNOWN';
  }
}
