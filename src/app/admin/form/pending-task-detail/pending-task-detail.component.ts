import { Component, OnInit } from '@angular/core';
import {JobService} from "../../services/job.service";
import {ActivatedRoute, Router} from "@angular/router";
import {JobInfoDtoV3} from "../../models/job-info-dto-v3";
import {NzModalService} from "ng-zorro-antd/modal";
import {Skills_jobsModel} from "../../models/skills_jobs.model";
import {Locations_jobModel} from "../../models/locations_job.model";
import {LevelsJobModel} from "../../models/levels-job.model";
import {GlobalConstants} from "../../../constants/GlobalConstants";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PendingTaskService} from "../../services/pending-task.service";

@Component({
  selector: 'app-pending-task-detail',
  templateUrl: './pending-task-detail.component.html',
  styleUrls: ['./pending-task-detail.component.css']
})
export class PendingTaskDetailComponent implements OnInit {

  constructor(private jobService: JobService, private route: ActivatedRoute, private router: Router, private modalService: NzModalService, private fb: FormBuilder, private pendingTaskService: PendingTaskService) { }

  isSpinning = false;
  jobInfo?: JobInfoDtoV3;
  domain = GlobalConstants.DOMAIN;
  formConfigJob!: FormGroup;
  submitting = false;
  resultCheck = false;

  ngOnInit(): void {
    this.loadJobPendingDetail();
    this.formConfigJob = this.fb.group({
      reward: [0,Validators.required],
      priorityJob: [0,Validators.required],
      pendingTaskId: [this.route.snapshot.params['pendingTaskId'], Validators.required],
      wfStatusCd:[null, Validators.required]
    })
  }

  loadJobPendingDetail() {
    this.isSpinning = true;
    this.jobService.getJobPendingDetail(this.route.snapshot.params['pendingTaskId']).subscribe(value => {
      this.isSpinning = false;
      if (value.status == 200) {
        this.jobInfo = value.data;
        this.formConfigJob.controls['reward'].setValue(this.jobInfo?.reward);
        this.formConfigJob.controls['priorityJob'].setValue(this.jobInfo?.priorityJob);
      } else {
        this.modalService.error({
          nzTitle: 'Lỗi',
          nzContent: value.message,
          nzOkText: 'OK'
        });
      }
    }, error => {
      this.isSpinning = false;
      this.modalService.error({
        nzTitle: 'Lỗi',
        nzContent: 'Có lỗi xảy ra, vui lòng thử lại sau!',
        nzOkText: 'OK',
        nzOnOk: () => {
          this.router.navigate(['/pending-task'])
        }
      });
    })
  }

  current = 0;

  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    this.current += 1;
    this.changeContent();
  }

  submitData(wfStatusCd: string): void {
    this.submitting = true;
    this.isSpinning = true;
    this.formConfigJob.controls['wfStatusCd'].setValue(wfStatusCd);
    this.pendingTaskService.approvePendingTaskJob(this.formConfigJob.getRawValue()).subscribe(value => {
      if (value.status == 200) {
        this.submitting = false;
        this.isSpinning = false;
        this.current = 2;
        this.resultCheck = true;
      } else {
        this.resultCheck = false;
      }
    }, error => {
      this.submitting = false;
      this.isSpinning = false;
      this.current = 2;
      this.resultCheck = false;
    }, () => {
      this.submitting = false;
      this.isSpinning = false;
      this.current = 2;
    })
  }

  done(): void {
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        break;
      }
      case 1: {
        break;
      }
      case 2: {
        break;
      }
    }
  }

  getListSkill(skills?: Skills_jobsModel[]) {
    return skills?.map(item => item.name).join(", ");
  }

  getLocation(locations?: Locations_jobModel[]) {
    return locations?.map(item => item.name).join(", ");
  }

  getLevel(levels?: LevelsJobModel[]) {
    return levels?.map(item => item.name).join(", ");
  }

  formatterVND = (value: number): string => `đ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  parserVND = (value: string): string => value!.replace(/đ\s?|(,*)/g, '');

}
