import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {Observable, Observer} from "rxjs";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {AccountCompanyService} from "../account-company.service";
import {CompanyBenefit} from "../account-company.models";
import {GlobalConstants} from "../../../../constants/GlobalConstants";
import {AppParams} from "../../../../constants/app-params";

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {
  sizeCompany = AppParams.LIST_SIZE_COMPANY;
  careerCompany = AppParams.LIST_CAREER_COMPANY;
  formCompanyProfile = new FormGroup({});
  formCompanyBenefits = new FormGroup({});
  formCompanyIdentity = new FormGroup({});
  formCompanyGallery = new FormGroup({});
  formCompanySocialSharingDetail = new FormGroup({});
  formCompanyRepresentativeInfo = new FormGroup({});
  loading = false;
  listBenefit: CompanyBenefit[] | undefined;


  urlUpload: string | undefined;
  urlUploadGallery: string | undefined;
  logoCompany: string | undefined;
  idLogoCompany: string | undefined;

  imgThumbnail: any | undefined;
  idThumbnail: any | undefined;
  fileImgCompany: any;
  fileImgThumbnail: any;


  previewImage: string | undefined = '';
  previewVisible = false;
  profileCompany : any;
  loadingForm = true;

  imgListGallery: any = [
  ];

  constructor(
    private accountCompanyService: AccountCompanyService,
    private fb: FormBuilder,
    private msg: NzMessageService
  ) {
    this.urlUpload = GlobalConstants.API_ENDPOINT + 'file-management/upload-logo';
    this.urlUploadGallery = GlobalConstants.API_ENDPOINT + 'file-management/upload';
  }

  ngOnInit(): void {
    this.initFormControl();

  }

  initInfo() {
    this.accountCompanyService.getAllCompanyBenefit().subscribe((data: any) => {
      this.listBenefit = data.data;
      this.formCompanyBenefits = this.fb.group({
        benefitDetails: this.fb.array([])
      });
      if (this.listBenefit) {
        for (let dataBe of this.listBenefit) {
          if (this.profileCompany && this.profileCompany.benefitDetails) {
            for (const bene of this.profileCompany.benefitDetails) {
              if (bene.benefitId == dataBe.id) {
                dataBe.detail = bene.detail;
                dataBe.benefitCheckbox = true;
              }
            }
          }
          this.addBenefit(dataBe);
        }
      }
      this.loadingForm = false;
    });

    this.accountCompanyService.getAllCompanyIndustry().subscribe((data: any) => {
      this.careerCompany = data.data;
    });
  }

  addBenefit(benefit: CompanyBenefit) {
    const benefitForm = this.fb.group({
        benefitCheckbox: [benefit.benefitCheckbox],
        detail: [benefit.detail],
        benefitId: [benefit.id],
        id: [benefit.id],
        name: [benefit.name],
        icon: [benefit.icon]
      });
    this.benefits.push(benefitForm);
  }

  initFormControl(): void {
    this.accountCompanyService.adminGetCompanyProfile().subscribe((dataC: any) => {
      this.profileCompany = dataC.data;
      this.initInfo();
      const ccIndustry = this.profileCompany.companyIndustries.map((cc: any) => cc.id);
      this.profileCompany.viewLinkLogo = GlobalConstants.API_ENDPOINT_URL + this.profileCompany.viewLinkLogo;
      this.profileCompany.viewLinkThumbnail = GlobalConstants.API_ENDPOINT_URL + this.profileCompany.viewLinkThumbnail;
      this.fileImgCompany = [{
        uid: '-1',
        name: 'xx.png',
        status: 'done',
        url: this.profileCompany.viewLinkLogo
      }];
      this.fileImgThumbnail = [{
        uid: '-2',
        name: '',
        status: 'done',
        url: this.profileCompany.viewLinkThumbnail
      }];
      if (this.profileCompany?.gallery && this.profileCompany?.gallery?.length) {
        let i = 0;
        for (const ga of this.profileCompany.gallery) {
          const ig = {
            uid: --i + '',
            name: 'xx.png',
            status: 'done',
            url: GlobalConstants.API_ENDPOINT_URL + ga.viewLink
          };
          this.imgListGallery.push(ig);
          i--;
        }
      }
      console.log(this.profileCompany);
      console.log(this.imgListGallery);


      this.formCompanyProfile = this.fb.group({
        companyName: [this.profileCompany.companyName, Validators.required],
        companyPhone: [this.profileCompany.companyPhone],
        companyAddress: [this.profileCompany.companyAddress],
        companySize: [this.profileCompany.companySize],
        companyEmail: [this.profileCompany.companyEmail,Validators.required],
        ccMail: [this.profileCompany.ccMail],
        website: [this.profileCompany.website,Validators.required],
        companyIndustries: [ccIndustry],
        workingHours: [this.profileCompany.workingHours]
      });
      this.formCompanyIdentity = this.fb.group({
        description: [this.profileCompany.description]
      });
      this.formCompanySocialSharingDetail = this.fb.group({
        linkedIn: [this.profileCompany.linkedIn,Validators.required],
        facebook: [this.profileCompany.facebook,Validators.required],
        thumbnailDescription: [this.profileCompany.thumbnailDescription,Validators.required],
      });
      this.formCompanyRepresentativeInfo = this.fb.group({
        firstNameRepresentative: [this.profileCompany.firstNameRepresentative],
        lastNameRepresentative: [this.profileCompany.lastNameRepresentative],
        headline: [this.profileCompany.headline],
        emailRepresentative: [this.profileCompany.emailRepresentative,Validators.required],
        phoneRepresentative: [this.profileCompany.phoneRepresentative,Validators.required]
      });
    });
  }

  t() {
    console.log(this);
  }

  beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]): Observable<boolean> =>
    new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.msg.error('You can only upload JPG file!');
        observer.complete();
        return;
      }
      const isLt2M = file.size! / 1024 / 1024 < 2;
      if (!isLt2M) {
        this.msg.error('Image must smaller than 2MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt2M);
      observer.complete();
    });

  handleChange(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        break;
      case 'done':
        this.logoCompany = GlobalConstants.API_ENDPOINT_URL + info.file.response.data[0].viewLink;
        this.idLogoCompany = info.file.response.data[0].id;
        break;
      case 'error':
        this.msg.error('Network error');
        this.loading = false;
        break;
    }
  }

  handleChangeThumbnail(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        break;
      case 'done':
        this.imgThumbnail = GlobalConstants.API_ENDPOINT_URL + info.file.response.data[0].viewLink;
        this.idThumbnail = info.file.response.data[0].id;
        break;
      case 'error':
        this.imgThumbnail = null;
        this.msg.error('Network error');
        this.loading = false;
        break;
    }
  }

  handleChangeGallery(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        break;
      case 'done':
        // this.logoCompany = GlobalConstants.API_ENDPOINT_URL + '/api/v1/file-management/view/' + info.file.response.data[0].id;
        break;
      case 'error':
        this.msg.error('Network error');
        this.loading = false;
        break;
    }
  }

  get benefits() {
    return this.formCompanyBenefits.controls["benefitDetails"] as FormArray;
  }

  submitSaveOrUpdate() {
    const payload = {
      ...this.formCompanyProfile.value,
      ...this.formCompanyBenefits.value,
      ...this.formCompanySocialSharingDetail.value,
      ...this.formCompanyIdentity.value,
      ...this.formCompanyRepresentativeInfo.value
    }

    if (payload.benefitDetails && payload.benefitDetails.length) {
      payload.benefitDetails = payload.benefitDetails.filter((benefit: any) => !!benefit.benefitCheckbox);
    }

    payload.fileLogoId = this.idLogoCompany ? this.idLogoCompany : this.profileCompany.fileLogoId;
    payload.thumbnailLogoId = this.idThumbnail ? this.idThumbnail : this.profileCompany.thumbnailLogoId;
    payload.gallery = [];
    if (this.imgListGallery && this.imgListGallery.length) {
      this.imgListGallery.forEach((res: any) => {
        return payload.gallery.push(res.response.data[0].id);
      });
    }
    console.log(payload);
    this.accountCompanyService.adminCreateCompanyProfile(payload).subscribe((data) => console.log(data));
  }

  handlePreview = async (file: any): Promise<void> => {
    if (!file.url && !file.preview) {
      file.preview = await this.getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  getBase64 = (file: File): Promise<string | ArrayBuffer | null> =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
}
