import {Component, OnInit} from '@angular/core';
import {AdminService} from "../../../services/admin.service";
import {NzUploadChangeParam} from "ng-zorro-antd/upload/interface";
import {GlobalConstants} from "../../../../constants/GlobalConstants";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'app-slides-show-company',
  templateUrl: './slides-show-company.component.html',
  styleUrls: ['./slides-show-company.component.css']
})
export class SlidesShowCompanyComponent implements OnInit {
  isVisible = false;
  isConfirmLoading = false;
  urlUpload: string | undefined;

  data = [
    'Racing car sprays burning fuel into crowd.',
    'Japanese princess to wed commoner.',
    'Australian walks 100km after outback crash.',
    'Man charged over missing wedding girl.',
    'Los Angeles battles huge wildfires.'
  ];

  constructor(
    public adminService: AdminService,
    private msg: NzMessageService
  ) {
  }

  ngOnInit(): void {
    this.urlUpload = GlobalConstants.API_ENDPOINT + 'file-management/upload-logo';
  }

  handleOk() {
  }

  handleCancelEdit() {
  }

  handleOkEdit() {
    this.isConfirmLoading = true;

    // do update userInfo
    /*this.levelsJobService.adminEditUser(this.formEdit.getRawValue()).subscribe((next) => {
      if (next.status == 200) {
        this.modalService.success({
          nzTitle: 'Thông báo',
          nzContent: 'Sửa trình độ thành công',
          nzOkText: 'OK'
        });
        this.isVisibleEdit = false;
        this.formEdit.reset();
        this.loadingUserInfo();
      }*/
  }

  handleChange({ file, fileList }: NzUploadChangeParam): void {
    const status = file.status;
    if (status !== 'uploading') {
      console.log(file, fileList);
    }
    if (status === 'done') {
      this.msg.success(`${file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      this.msg.error(`${file.name} file upload failed.`);
    }
  }

}
