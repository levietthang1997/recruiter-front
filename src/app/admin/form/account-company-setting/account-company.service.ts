import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {CompanyBenefit, CompanyIndustry} from "./account-company.models";
import {GlobalConstants} from "../../../constants/GlobalConstants";
import {BasePagingResponseModel} from "../../models/base_paging_response_model";
import {BaseResponseModel} from "../../models/base_response.model";

@Injectable({
  providedIn: 'root',
})
export class AccountCompanyService {

  private readonly API_INDUSTRY_INFO_NAME = 'company-industry';
  private readonly API_COMPANY_BENEFIT_NAME = 'company-benefit';
  private readonly API_COMPANY_PROFILE = 'company';

  constructor(private http: HttpClient) {
  }

  getAllCompanyIndustry(): Observable<BasePagingResponseModel<CompanyIndustry[]>> {
    return this.http
      .get(GlobalConstants.API_ENDPOINT + this.API_INDUSTRY_INFO_NAME + "/get-all")
      .pipe(
        map((response:BasePagingResponseModel<CompanyIndustry[]>) => {
          return response;
        })
      )
  }

  adminCreateCompanyIndustry(value: CompanyIndustry): Observable<BasePagingResponseModel<CompanyIndustry>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_INDUSTRY_INFO_NAME + "/create", value);
  }

  adminDeleteCompanyIndustry(value: { listId: number[] }) {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_INDUSTRY_INFO_NAME + "/delete", value);
  }

  adminEditCompanyIndustry(value: CompanyIndustry): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_INDUSTRY_INFO_NAME + "/update", value)
  }

  getAllCompanyBenefit(): Observable<BasePagingResponseModel<CompanyBenefit[]>> {
    return this.http
      .get(GlobalConstants.API_ENDPOINT + this.API_COMPANY_BENEFIT_NAME + "/get-all")
      .pipe(
        map((response:BasePagingResponseModel<CompanyBenefit[]>) => {
          return response;
        })
      )
  }

  adminCreateCompanyBenefit(value: CompanyBenefit): Observable<BasePagingResponseModel<CompanyBenefit>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_COMPANY_BENEFIT_NAME + "/create", value);
  }

  adminDeleteCompanyBenefit(value: { listId: number[] }) {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_COMPANY_BENEFIT_NAME + "/delete", value);
  }

  adminEditCompanyBenefit(value: CompanyBenefit): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + this.API_COMPANY_BENEFIT_NAME + "/update", value)
  }

  adminCreateCompanyProfile(value: any): Observable<BaseResponseModel<Object>> {
    return this.http.post(GlobalConstants.API_ENDPOINT + "company/update-profile", value)
  }

  adminGetCompanyProfile() {
    return this.http.get(GlobalConstants.API_ENDPOINT + this.API_COMPANY_PROFILE + "/profile")
  }
}
