
export interface CompanyIndustry {
  id: number,
  name: string
}

export interface CompanyBenefit {
  id: number,
  name: string,
  icon: string,
  detail?: string,
  benefitCheckbox?: boolean
}

export interface IconGallery {
  id: number,
  name: string,
  viewLink: string
}
