import { Component, OnInit } from '@angular/core';
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-account-company-setting',
  templateUrl: './account-company-setting.component.html',
  styleUrls: ['./account-company-setting.component.css']
})
export class AccountCompanySettingComponent implements OnInit {

  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {
    this.route.navigate(['account-company-setting'], {queryParams: {tab: 'Company-Profile'}})
  }

}
