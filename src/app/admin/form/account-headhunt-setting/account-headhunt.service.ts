import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {GlobalConstants} from "../../../constants/GlobalConstants";
import {BasePagingResponseModel} from "../../models/base_paging_response_model";

@Injectable({
  providedIn: 'root',
})
export class AccountHeadhuntService {

  private readonly API_HEADHUNT_PROFILE = 'headhunt';
  private readonly API_USER_INFO = 'user-info';

  constructor(private http: HttpClient) {
  }

  adminGetHeadhuntProfile() {
    return this.http.get<any>(GlobalConstants.API_ENDPOINT + this.API_HEADHUNT_PROFILE + "/profile");
  }

  adminGetAllSkill() {
    return this.http.get<any>(GlobalConstants.API_ENDPOINT + "skills/all");
  }

  adminGetAllLocation() {
    return this.http.get<any>(GlobalConstants.API_ENDPOINT + "location/all");
  }

  adminGetAllLevel() {
    return this.http.get<any>(GlobalConstants.API_ENDPOINT + "level/all");
  }

  adminGetAllJobRole() {
    return this.http.get<any>(GlobalConstants.API_ENDPOINT + "job-role/all");
  }

  updateProfileHeadhunt(payload: any) {
    return this.http.post<any>(GlobalConstants.API_ENDPOINT + this.API_HEADHUNT_PROFILE + "/update-profile", payload);
  }

  updateAccountHr(payload: any) {
    return this.http.post<any>(GlobalConstants.API_ENDPOINT + this.API_USER_INFO + "/change-password", payload);
  }
}
