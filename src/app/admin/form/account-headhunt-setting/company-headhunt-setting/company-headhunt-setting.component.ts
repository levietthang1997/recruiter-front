import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {AccountHeadhuntService} from "../account-headhunt.service";
import {GlobalConstants} from "../../../../constants/GlobalConstants";
import {NzUploadFile} from "ng-zorro-antd/upload";
import {Observable, Observer} from "rxjs";

@Component({
  selector: 'app-company-headhunt-setting',
  templateUrl: './company-headhunt-setting.component.html',
  styleUrls: ['./company-headhunt-setting.component.css']
})
export class CompanyHeadhuntSettingComponent implements OnInit {
  formApplicationHR = new FormGroup({});
  formAccountHR = new FormGroup({});
  urlUpload: string | undefined;
  loading = true;
  logoHeadhunt: any;
  idLogoHeadhunt: any;
  profileHeadhunt: any;

  listOfJobRole: any;
  genderList = [
    {
      id: "NONE",
      name: "Không công khai"
    },
    {
      id: "MALE",
      name: "Nam"
    },
    {
      id: "FEMALE",
      name: "Nữ"
    }
  ];

  constructor(
    private msg: NzMessageService,
    private accountHeadhuntService: AccountHeadhuntService,
    private fb: FormBuilder
  ) {

    this.urlUpload = GlobalConstants.API_ENDPOINT + 'file-management/upload-logo';
  }

  ngOnInit(): void {
    this.initFormBuilder();
  }

  initFormBuilder() {
    this.accountHeadhuntService.adminGetHeadhuntProfile().subscribe(dataH => {
      this.profileHeadhunt = dataH.data;
      this.profileHeadhunt.avatarUrl = this.profileHeadhunt.avatarUrl ? GlobalConstants.API_ENDPOINT_URL + this.profileHeadhunt.avatarUrl : null;
      this.initInfo();
      this.formApplicationHR = this.fb.group({
        avatarId: [this.profileHeadhunt.avatarId, Validators.required],
        avatarUrl: [this.profileHeadhunt.avatarUrl],
        firstName: [this.profileHeadhunt.firstName],
        lastName: [this.profileHeadhunt.lastName],
        hotline: [this.profileHeadhunt.hotline],
        email: [this.profileHeadhunt.email,Validators.required],
        phone: [this.profileHeadhunt.phone],
        address: [this.profileHeadhunt.address,Validators.required],
        gender: [this.profileHeadhunt.gender],
        linkedIn: [this.profileHeadhunt.linkedIn],
        introduction: [this.profileHeadhunt.introduction]
      });
      this.formAccountHR = this.fb.group({
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      });
      this.loading = false;
    })
  }

  initInfo() {
    this.accountHeadhuntService.adminGetAllJobRole().subscribe((data: any) => {
      this.listOfJobRole = data.data;
    });
  }

  submitSaveOrUpdatePassword() {
    const payload = this.formAccountHR.value;
    this.accountHeadhuntService.updateAccountHr(payload).subscribe((data: any) => {
      this.msg.success('Thay đổi mật khẩu thành công!');
    });
  }

  submitSaveOrUpdateHeadhunt() {
    const payload = this.formApplicationHR.value;
    payload.avatarId = this.idLogoHeadhunt;
    this.accountHeadhuntService.updateProfileHeadhunt(payload).subscribe((data: any) => {
      this.msg.success('Lưu dữ liệu hồ sơ công ty thành công!');
    });
  }

  t() {
    console.log(this);
  }

  beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]): Observable<boolean> =>
    new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.msg.error('You can only upload JPG file!');
        observer.complete();
        return;
      }
      const isLt2M = file.size! / 1024 / 1024 < 2;
      if (!isLt2M) {
        this.msg.error('Image must smaller than 2MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt2M);
      observer.complete();
    });

  handleChange(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        break;
      case 'done':
        this.logoHeadhunt = GlobalConstants.API_ENDPOINT_URL + info.file.response.data[0].viewLink;
        this.idLogoHeadhunt = info.file.response.data[0].id;
        this.msg.error('Upload ảnh thành công!');
        console.log(info);
        break;
      case 'error':
        this.msg.error('Network error');
        break;
    }
  }

}
