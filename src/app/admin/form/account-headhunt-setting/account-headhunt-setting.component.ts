import { Component, OnInit } from '@angular/core';
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-account-headhunt-setting',
  templateUrl: './account-headhunt-setting.component.html',
  styleUrls: ['./account-headhunt-setting.component.scss']
})
export class AccountHeadhuntSettingComponent implements OnInit {

  constructor(
    private route: Router
  ) { }

  ngOnInit(): void {
    this.route.navigate(['account-headhunt-setting'], {queryParams: {tab: 'Company-Profile'}})
  }

}
