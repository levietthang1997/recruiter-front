import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import * as selectOptions from "../../../models/constant/select.constant";
import { differenceInCalendarDays } from 'date-fns';
import { en_US, NzI18nService } from 'ng-zorro-antd/i18n';
import {GlobalConstants} from "../../../../constants/GlobalConstants";
import {CandidateModalTemplateComponent} from "../candidate-modal-template/candidate-modal-template.component";
import {NzDrawerService} from "ng-zorro-antd/drawer";
import {FooterDrawerComponent} from "../footer-drawer/footer-drawer.component";
import {ActivatedRoute} from "@angular/router";
import {concatMap, tap} from "rxjs";
import {CandidateModalDetailComponent} from "../candidate-modal-detail/candidate-modal-detail.component";

@Component({
  selector: 'app-candidates-list',
  templateUrl: './candidates-list.component.html',
  styleUrls: ['./candidates-list.component.scss']
})
export class CandidatesListComponent implements OnInit {
  formSearchCandidate = new FormGroup({});
  @ViewChild('footer') footer: TemplateRef<Element> | undefined;
  @ViewChild('footerView') footerView: TemplateRef<Element> | undefined;
  @ViewChild('extra') extra: TemplateRef<Element> | undefined;
  provinceData = ['Đang tìm việc ', 'Đã có việc', 'Passive'];
  statusUV = 'Passive';
  today = new Date();
  urlUpload?: string;

  jobTitle = selectOptions.jobTitle;
  classifyJob = selectOptions.classifyJob;
  locations = selectOptions.locations;
  dueDateJob = selectOptions.dueDateJob;
  skillJob = selectOptions.skillJob;
  vm$ = this.activeRoute.queryParams.pipe(
    tap((data: any) => {
      console.log(data);
    })
  );

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private i18n: NzI18nService,
    private drawerService: NzDrawerService
  ) {
    this.i18n.setLocale(en_US);
    this.urlUpload = GlobalConstants.API_ENDPOINT + 'file-management/upload-logo';
  }

  ngOnInit(): void {
    this.initFormGroup();
  }

  initFormGroup() {
    this.formSearchCandidate = this.fb.group({
      location: ['']
    })
  }

  disabledDate = (current: Date): boolean =>
    // Can not select days before today and today
    differenceInCalendarDays(current, this.today) > 0;

  handleChange(e: any): void {
    console.log(e);
  }

  openCreateCV() {

  }

  showModal2(): void {

    const drawerRef = this.drawerService.create<CandidateModalTemplateComponent, { value: string }, string>({
      nzTitle: 'Thêm ứng viên',
      nzFooter: this.footer,
      nzWidth: 1200,
      nzContent: CandidateModalTemplateComponent,
      nzContentParams: {
        mode: 1
      }
    });
    drawerRef.afterOpen.subscribe(() => {
      console.log('Drawer(Template) open');
    });

    drawerRef.afterClose.subscribe(() => {
      console.log('Drawer(Template) close');
    });
  }

  showModal3(): void {
    const drawerRef = this.drawerService.create({
      nzTitle: 'Hồ sơ ứng viên',
      nzFooter: this.footerView,
      nzExtra: this.extra,
      nzWidth: 1200,
      nzContent: CandidateModalDetailComponent,
      nzContentParams: {
        mode: 'view'
      }
    });
    drawerRef.afterOpen.subscribe(() => {
      console.log('Drawer(Template) open');
    });

    drawerRef.afterClose.subscribe(() => {
      console.log('Drawer(Template) close');
    });
  }

  handleOk(): void {
  }

}
