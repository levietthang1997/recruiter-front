import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CandidateService} from "../../../services/candidate.service";
import {LocationsJobService} from "../../../services/locations-job.service";

@Component({
  selector: 'app-candidate-modal-detail',
  templateUrl: './candidate-modal-detail.component.html',
  styleUrls: ['./candidate-modal-detail.component.scss']
})
export class CandidateModalDetailComponent implements OnInit {

  tabs = ['1', '2'];
  timeToWork = [
    {
      id: 1,
      name: 'Có thể nhận việc ngay'
    },
    {
      id: 2,
      name: 'Sau 1 tháng'
    },
    {
      id: 3,
      name: 'Sau 2 tháng'
    },
    {
      id: 4,
      name: 'Sau 3 tháng'
    },
  ];
  classify = [
    {
      id: null,
      name: 'Tất cả'
    },
    {
      id: 1,
      name: 'IT'
    },
    {
      id: 2,
      name: 'Non-IT'
    }
  ];
  categoryJob = [
    {
      id: 1,
      name: 'Java'
    },
    {
      id: 2,
      name: 'UX/UI Designer'
    },
    {
      id: 3,
      name: 'IT Sales'
    },
    {
      id: 4,
      name: 'Game Developer'
    },
  ];
  selectedIndex = 0;
  srcPdf: any;
  loadedPdf = false;
  formCandidate = new FormGroup({});
  locations: any;
  mode: any;

  constructor(
    private candidateService: CandidateService,
    private locationsJobService: LocationsJobService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.candidateService.downloadFile('https://bvofficeuat.baoviet.com.vn/api/tasks/group/getPdf?photo_path=/data/OfficeFile/file-path/2022/9/2/1662134063_178352.pdf')
      .subscribe((data: any) => {
        console.log(data)
        this.srcPdf = data;
        this.loadedPdf = true;
      });

    this.formCandidate = this.fb.group({
      lastName: ['', Validators.required],
      firstName: [''],
      email: [''],
      phone: ['',Validators.required],
      yearBirthday: [''],
      website: ['',Validators.required],
      title: [''],
      place: [''],
      category: [''],
      workStart: [''],
      type: [''],
      negotiation: [''],
      salaryFrom: [''],
      salaryTo: [''],
      description: ['']
    });

    this.locationsJobService.getAllLocationInfo().subscribe((data: any) => {
      this.locations = data.data;
    });

  }

  closeTab({ index }: { index: number }): void {
    this.tabs.splice(index, 1);
  }

  newTab(): void {
    this.tabs.push('New Tab');
    this.selectedIndex = this.tabs.length;
  }

  changeMode() {
    this.mode = 'edit';
  }
  formatterVND = (value: number): string => value ? `đ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '0';
  parserVND = (value: string): string => value!.replace(/đ\s?|(,*)/g, '');

}
