import {Component, OnInit} from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {GlobalConstants} from "../constants/GlobalConstants";
import {JwtService} from "./services/jwt.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  isCollapsed = false;
  roles: any;
  typeUser: number = 0;

  constructor(
    private keycloak: KeycloakService,
  ) { }

  ngOnInit(): void {
    this.roles = this.keycloak.getUserRoles();
    if (this.roles.includes('admin')) {
      this.typeUser = 1;
    } else if (this.roles.includes('headhunt')) {
      this.typeUser = 2;
    } else if (this.roles.includes('company')) {
      this.typeUser = 3;
    }
  }

  doLogout() {
    this.keycloak.logout(GlobalConstants.IS_PROD ? "https://app.kttk.xyz" : "http://localhost:4200")
  }

  t() {
    console.log(this.keycloak.getUserRoles());
  }
}
