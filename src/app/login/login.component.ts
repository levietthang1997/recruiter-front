import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailLogin: string = '';
  passwordLogin: string = '';
  isSpinning = false;

  constructor(protected router: Router, protected keycloak: KeycloakService) {
  }


  async ngOnInit(): Promise<void> {
    if (await this.keycloak.isLoggedIn())
      this.router.navigate(['/admin']);
  }


  doLogin() {
    // this.isSpinning = true;
    this.keycloak.login();
  }
  doRegister() {
    this.router.navigate(["/register"])
  }

}
