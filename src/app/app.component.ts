import {Component, OnInit} from '@angular/core';
import {KeycloakEventType, KeycloakService} from "keycloak-angular";
import {JwtService} from "./admin/services/jwt.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = '';

  constructor(
    private keycloakService: KeycloakService,
    private jwtService: JwtService
  ) {
  }

  ngOnInit(): void {
    this.keycloakService.keycloakEvents$.subscribe({
      next: (e) => {
        if (e.type == KeycloakEventType.OnTokenExpired) {
          this.keycloakService.updateToken().then(r => {
            if (r) {
              this.keycloakService.getToken().then(data => this.jwtService.token.next(data)).catch(console.log);
            }
          }).catch(reason => {
            location.reload();
          })
        }
      }
    });
  }
}
